<?php

require_once 'hewan.php';

class Harimau
{
    use Hewan;
    use Fight;

    public function __construct($nama1 = 'Elang', $nama2 = 'Harimau', $jumlahKaki = 4, $keahlian = "lari cepat", $attackPower = 7, $deffencePower = 8)
    {
        $this->nama1 = $nama1;
        $this->nama2 = $nama2;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
}
