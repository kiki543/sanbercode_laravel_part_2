<?php

trait Hewan
{
    public $nama,
        $darah = 50,
        $jumlahKaki,
        $keahlian;

    public function atraksi()
    {
        return $this->getName() . ' sedang ' . $this->keahlian;
    }

    abstract public function getName();
}
