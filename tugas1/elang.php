<?php

require_once 'hewan.php';

class Elang
{
    use Hewan;
    use Fight;

    public function __construct($nama1 = 'Elang', $nama2 = 'Harimau', $jumlahKaki = 2, $keahlian = "terbang tinggi", $attackPower = 10, $deffencePower = 5)
    {
        $this->nama1 = $nama1;
        $this->nama2 = $nama2;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
}
